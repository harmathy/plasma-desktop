#!/bin/bash

set -e
set -x

THIS="$(basename "$0")"
HERE="$(cd "$(dirname "$0")"; cd "$(pwd -P)"; pwd)"

TARGET="$1"

test -d "$TARGET"

(
  cd "$TARGET"
  cmake -DMAKE_INSTALL_PREFIX=/usr "$HERE"
  make
  sudo make install
)



