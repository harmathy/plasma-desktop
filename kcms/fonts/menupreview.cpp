/*
 * Copyright 2018 Max Harmathy
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "menupreview.h"
#include "freetype-renderer.h"

#if defined(HAVE_FONTCONFIG)

#include <QApplication>
#include <QGridLayout>
#include <QIcon>
#include <QPainter>
#include <QtMath>

namespace
{
static const int MAX_PREVIEW_WIDTH = 120;
static const int MAX_PREVIEW_HIGHT = 240;
}

PreviewParameters::PreviewParameters(const QString& fontFamily,
                                     double pointSize,
                                     KXftConfig* options)
    : fontFamily(fontFamily), pointSize(pointSize), options(options)
{
}

PreviewParameters::PreviewParameters(const PreviewParameters& value)
    : PreviewParameters(value.fontFamily, value.pointSize, new KXftConfig(*value.options))
{
}

PreviewParameters::~PreviewParameters()
{
    delete options;
}

PreviewParameters PreviewParameters::fromString(const QString& id)
{
    KXftConfig* options = new KXftConfig();
    auto fragments = id.split("/");
    QString fontFamily = "";
    double pointSize = 10;
    if (fragments.length() <= 5) {
        fontFamily = fragments[0];
        pointSize = fragments[1].toFloat();
        options->setAntiAliasing(
            static_cast<KXftConfig::AntiAliasing::State>(fragments[2].toInt()));
        options->setHintStyle(static_cast<KXftConfig::Hint::Style>(fragments[3].toInt()));
        options->setSubPixelType(static_cast<KXftConfig::SubPixel::Type>(fragments[4].toInt()));
    }
    return PreviewParameters(fontFamily, pointSize, options);
}

QString PreviewParameters::toFormatetString()
{
    auto typeface = QString("%1 %2").arg(fontFamily).arg(pointSize);

    auto aa = options->getAntiAliasing() == KXftConfig::AntiAliasing::Disabled;
    auto antialiasing = aa ? "Disabled" : "Enabled";

    KXftConfig::Hint::Style hintData;
    options->getHintStyle(hintData);
    auto hint = KXftConfig::toStr(hintData);

    KXftConfig::SubPixel::Type subpixelData;
    options->getSubPixelType(subpixelData);
    auto subpixel = KXftConfig::toStr(subpixelData);

    return QString("Typeface:\t%1\nAnti-Aliasing:\t%2\nHinting Style:\t%3\nSub-Pixel Order:\t%4")
        .arg(typeface, antialiasing, hint, subpixel);
}

EntryMockup::EntryMockup(const QString& label, const QString& iconName)
    : label{ label }, iconName{ iconName }
{
}

const QString& EntryMockup::getIconName() const
{
    return iconName;
}

const QString& EntryMockup::getLabel() const
{
    return label;
}

void MenuMockup::add(const EntryMockup& item)
{
    entries.append(item);
}

QString MenuMockup::getLabel(int index) const
{
    return entries.at(index).getLabel();
}

QString MenuMockup::getIconName(int index) const
{
    return entries.at(index).getIconName();
}

MenuMockup MenuMockup::basicExample()
{
    MenuMockup result;
    result.add(EntryMockup("Office", "applications-office"));
    result.add(EntryMockup("Internet", "applications-internet"));
    result.add(EntryMockup("Multimedia", "applications-multimedia"));
    result.add(EntryMockup("Graphics", "applications-graphics"));
    result.add(EntryMockup("Accessories", "applications-accessories"));
    result.add(EntryMockup("Development", "applications-development"));
    result.add(EntryMockup("Settings", "preferences-system"));
    result.add(EntryMockup("System", "applications-system"));
    result.add(EntryMockup("Utilities", "applications-utilities"));
    return result;
}

int MenuMockup::length() const
{
    return entries.length();
}

MenuPreviewRenderer::MenuPreviewRenderer(const QColor& background, int iconSize, int padding)
    : renderer(), iconSize(iconSize), padding(padding), background(background)
{
}

QImage MenuPreviewRenderer::getImage(const PreviewParameters& parameters)
{
    const auto menu = MenuMockup::basicExample();
    QList<QImage> lables;
    QList<QIcon> icons;
    QSize dimensions(0, 2 * padding);
    for (int i = 0; i < menu.length(); ++i) {
        auto image = renderer.renderText(menu.getLabel(i).toLocal8Bit(),
                                         parameters.fontFamily.toLocal8Bit(), parameters.pointSize,
                                         parameters.options, background, Qt::black);
        dimensions.rheight() += qMax(image.height(), iconSize) + 2 * padding;
        dimensions.setWidth(qMax(dimensions.width(), image.width()));
        lables.append(image);
        icons.append(QIcon::fromTheme(menu.getIconName(i)));
    }
    dimensions.rwidth() += iconSize + 4 * padding;
    QImage result(dimensions, QImage::Format_ARGB32);
    result.fill(background);
    QPainter p(&result);

    for (int i = 0, y = padding; i < menu.length(); ++i) {
        auto image = lables.at(i);
        auto icon = icons.at(i).pixmap(iconSize, iconSize);
        int heightOffset = (icon.height() - image.height()) / 2;
        bool iconIsSmaller = heightOffset < 0;
        if (iconIsSmaller) {
            heightOffset = (-heightOffset);
        }

        p.drawPixmap(QRectF(padding, y + (iconIsSmaller ? heightOffset : 0), iconSize, iconSize),
                     icon, QRectF(0, 0, iconSize, iconSize));

        p.drawImage(QRectF(iconSize + 3 * padding, y + (!iconIsSmaller ? heightOffset : 0),
                           image.width(), image.height()),
                    image, QRectF(0, 0, image.width(), image.height()));
        y += qMax(image.height(), iconSize) + 2 * padding;
    }
    p.end();
    return result;
}

void MenuPreviewButton::_update()
{
    auto parent = dynamic_cast<MenuPreviewArea*>(this->parent());
    auto image = parent->renderer.getImage(parameters);
    auto size = image.size();
    bool crop = false;
    if (size.width() > MAX_PREVIEW_WIDTH) {
        size.setWidth(MAX_PREVIEW_WIDTH);
        crop = true;
    }
    if (size.height() > MAX_PREVIEW_HIGHT) {
        size.setHeight(MAX_PREVIEW_HIGHT);
        crop = true;
    }
    if (crop) {
        image = image.copy(0, 0, size.width(), size.height());
    }
    setIcon(QPixmap::fromImage(image));
    setIconSize(size);
    setToolTip(parameters.toFormatetString());
}

MenuPreviewButton::MenuPreviewButton(const PreviewParameters& parameters, MenuPreviewArea* parent)
    : QPushButton(parent), parameters(parameters)
{
    setCheckable(true);
    _update();
}

KXftConfig::AntiAliasing::State MenuPreviewButton::getAntialiasing()
{
    return parameters.options->getAntiAliasing();
}

KXftConfig::Hint::Style MenuPreviewButton::getHint()
{
    KXftConfig::Hint::Style result;
    parameters.options->getHintStyle(result);
    return result;
}

KXftConfig::SubPixel::Type MenuPreviewButton::getSubPixel()
{
    KXftConfig::SubPixel::Type result;
    parameters.options->getSubPixelType(result);
    return result;
}

void MenuPreviewButton::setFont(const QString& fontFamily, double pointSize)
{
    parameters.fontFamily = fontFamily;
    parameters.pointSize = pointSize;
    _update();
}

void MenuPreviewArea::onSelectionChanged(QAbstractButton* button)
{
    if (MenuPreviewButton* my_button = dynamic_cast<MenuPreviewButton*>(button))
        emit selectionChanged(my_button->getAntialiasing(), my_button->getHint(),
                              my_button->getSubPixel());
}

void MenuPreviewArea::reset()
{
    auto button = group.checkedButton();
    if (button) {
        // temporarily remove excluve flag to make reset possible
        group.setExclusive(false);
        button->setChecked(false);
        group.setExclusive(true);
    }
}

MenuPreviewArea::MenuPreviewArea(const QString& fontFamily, double pointSize, QWidget* parent)
    : QWidget(parent)
    , fontFamily(fontFamily)
    , pointSize(pointSize)
    , renderer(QApplication::palette().background().color())
    , group()
{
    auto layout = new QGridLayout(this);
    setLayout(layout);

    // add buttons
    // start with anti-aliasing off
    auto options = new KXftConfig();
    options->setAntiAliasing(KXftConfig::AntiAliasing::Disabled);
    options->setHintStyle(KXftConfig::Hint::None);
    auto button = new MenuPreviewButton(PreviewParameters(fontFamily, pointSize, options), this);
    layout->addWidget(button, 0, 0);
    group.addButton(button);

    // without anti-alising there is only hinting 'on' or 'off', so set it to full
    options = new KXftConfig(*options);
    options->setHintStyle(KXftConfig::Hint::Full);
    button = new MenuPreviewButton(PreviewParameters(fontFamily, pointSize, options), this);
    layout->addWidget(button, 1, 0);
    group.addButton(button);

    int column = 1;
    int row = 1;
    // show subpixel off and RGB, which will fit in most cases
    // TODO dynamically select actual subpixel order of the display
    for (int i = 0; i < 2; ++i) {
        auto subpixel = i == 0 ? KXftConfig::SubPixel::None : KXftConfig::SubPixel::Rgb;
        // iterate over possible hinting settings
        for (int hint = KXftConfig::Hint::None; hint <= KXftConfig::Hint::Full; ++hint) {
            options = new KXftConfig();
            options->setAntiAliasing(KXftConfig::AntiAliasing::Enabled);
            options->setSubPixelType(subpixel);
            options->setHintStyle(static_cast<KXftConfig::Hint::Style>(hint));
            button = new MenuPreviewButton(PreviewParameters(fontFamily, pointSize, options), this);
            group.addButton(button);
            if (row == 0) {
                ++row;
            } else {
                --row;
                ++column;
            }
            layout->addWidget(button, row, column);
        }
    }
    for (auto button : group.buttons()) {
        auto my_button = dynamic_cast<MenuPreviewButton*>(button);
        connect(this, &MenuPreviewArea::fontChanged, my_button, &MenuPreviewButton::setFont);
    }
    group.setExclusive(true);

    connect(&group, SIGNAL(buttonClicked(QAbstractButton*)), this,
            SLOT(onSelectionChanged(QAbstractButton*)));
}

MenuPreviewArea::~MenuPreviewArea()
{
    for (auto button : group.buttons()) {
        delete button;
    }
}

void MenuPreviewArea::setFont(const QFont& font)
{
    fontFamily = font.family();
    pointSize = font.pointSizeF();
    emit fontChanged(fontFamily, pointSize);
}

void MenuPreviewArea::setFontFamily(const QFont& font)
{
    setFontFamily(font.family());
}

void MenuPreviewArea::setFontFamily(const QString& fontFamily)
{
    this->fontFamily = fontFamily;
    emit fontChanged(fontFamily, pointSize);
}

void MenuPreviewArea::setPointSize(double pointSize)
{
    this->pointSize = pointSize;
    emit fontChanged(fontFamily, pointSize);
}

#include "menupreview.moc"

#endif // defined(HAVE_FONTCONFIG)
