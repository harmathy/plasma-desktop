/*
 * Copyright 2018 Max Harmathy
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MENUPREVIEW_H
#define MENUPREVIEW_H

#include "freetype-renderer.h"
#include "kxftconfig.h"

#if defined(HAVE_FONTCONFIG)

#include <QButtonGroup>
#include <QImage>
#include <QList>
#include <QPushButton>
#include <QString>

/**
 * @brief The PreviewParameters is a helper class for communication between qml and
 * QQuickImageProvider.
 */
class PreviewParameters
{
public:
    QString fontFamily;
    double pointSize;
    KXftConfig* options;

    PreviewParameters(const QString& fontFamily, double pointSize, KXftConfig* options);
    PreviewParameters(const PreviewParameters& value);
    virtual ~PreviewParameters();
    static PreviewParameters fromString(const QString& id);
    QString toFormatetString();
};

class EntryMockup
{
    QString label;
    QString iconName;

public:
    EntryMockup(const QString& label, const QString& iconName);

    const QString& getIconName() const;
    const QString& getLabel() const;
};

class MenuMockup
{
    QList<EntryMockup> entries;

public:
    void add(const EntryMockup& item);
    QString getLabel(int index) const;
    QString getIconName(int index) const;
    static MenuMockup basicExample();
    int length() const;
};

class MenuPreviewArea;

class MenuPreviewRenderer
{
private:
    FreeTypeFontPreviewRenderer renderer;
    const int iconSize;
    const int padding;
    const QColor background;

public:
    MenuPreviewRenderer(const QColor& background, int iconSize = 16, int padding = 2);
    QImage getImage(const PreviewParameters& parameters);
};

class MenuPreviewButton : public QPushButton
{
    Q_OBJECT
private:
    PreviewParameters parameters;
    void _update();

public:
    MenuPreviewButton(const PreviewParameters& parameters, MenuPreviewArea* parent);
    KXftConfig::AntiAliasing::State getAntialiasing();
    KXftConfig::Hint::Style getHint();
    KXftConfig::SubPixel::Type getSubPixel();
public slots:
    void setFont(const QString& fontFamily, double pointSize);
};

class MenuPreviewArea : public QWidget
{
    Q_OBJECT
private:
    friend class MenuPreviewButton;

    QString fontFamily;
    double pointSize;

    MenuPreviewRenderer renderer;
    QButtonGroup group;

public:
    MenuPreviewArea(const QString& fontFamily, double pointSize, QWidget* parent = nullptr);
    virtual ~MenuPreviewArea();

public slots:
    void setFont(const QFont& font);
    void setFontFamily(const QFont& font);
    void setFontFamily(const QString& fontFamily);
    void setPointSize(double pointSize);
    void onSelectionChanged(QAbstractButton* button);
    void reset();

signals:
    void fontChanged(const QString& fontFamily, double pointSize);
    void selectionChanged(KXftConfig::AntiAliasing::State antiAliasing,
                          KXftConfig::Hint::Style hint,
                          KXftConfig::SubPixel::Type subPixel);
};

#endif // defined(HAVE_FONTCONFIG)

#endif // MENUPREVIEW_H
